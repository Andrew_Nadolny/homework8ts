import { callApi } from '../helpers/apiHelper';
import { getFighterById } from '../helpers/apiHelper';
import { Fighter } from '../fighter';


export async function getFighters() {
  try {
    const endpoint = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');
    
    return apiResult;
  } catch (error : Error | unknown) {
    throw error;
  }
}

export async function getFighterDetails(id : string) {
  return getFighterById(`details/fighter/${id}.json`) as Fighter;
}