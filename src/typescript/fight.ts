import { Fighter } from './fighter';

export function fight(firstFighter: Fighter, secondFighter: Fighter) : Fighter {
    let attaker = firstFighter;
    let defender = secondFighter;
    while(firstFighter.health > 0 && secondFighter.health > 0){
      defender.health -= getDamage(attaker, defender);
      let intermediateFighter = attaker;
      attaker = defender;
      defender = intermediateFighter;
    }
    return defender;
}

export function getDamage(attacker : Fighter, enemy : Fighter)  {
  let damage = getHitPower(attacker) - getBlockPower(enemy)
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter : Fighter) {
  let criticalHitChance : number = Math.random() + 1 ;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter : Fighter) {
  let dodgeChance : number = Math.random() + 1 ;
  return fighter.defense * dodgeChance;
}
