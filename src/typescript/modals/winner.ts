import { Fighter } from '../fighter';
import { showModal } from './modal';
import { createElement } from '../helpers/domHelper';


export  function showWinnerModal(fighter : Fighter) {
  const title = 'Winner info';
  const { name } = fighter;

  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });

  nameElement.innerText = `Name: ${name}`;

  bodyElement.append(nameElement);

  showModal({ title, bodyElement });
}

