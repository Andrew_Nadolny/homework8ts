import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { Fighter } from '../fighter';


export  function showFighterDetailsModal(fighter : Fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter : Fighter) {
  const { name, attack, defense, health, source } = fighter;



  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement = createElement({ tagName: 'p', className: 'fighter-name' });
  const attackElement = createElement({ tagName: 'p', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'p', className: 'fighter-defense' });
  const healthElement = createElement({ tagName: 'p', className: 'fighter-health' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes : { src: source, innerHeight: '100px' } });

  nameElement.innerText = `Name: ${name}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defence: ${defense}`;
  healthElement.innerText = `Health: ${health}`;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(imageElement);


  return fighterDetails;
}

export { createFighterDetails }
