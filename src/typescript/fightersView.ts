import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { Fighter } from './fighter';
import { getFighterDetails } from './services/fightersService';
import { fighters } from './helpers/mockData';

export function createFighters(fighters : Array<Fighter>) {
  const selectFighterForBattle = createFightersSelector();
  const fighterElements = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const fightersContainer = createElement({ tagName: 'div', className: 'fighters' });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache = new Map();

async function showFighterDetails(event : Event, fighter : Fighter) {
  const fullInfo = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId : string) : Promise<Fighter> {
  return await getFighterDetails(fighterId);
}

function createFightersSelector() {
  const selectedFighters = new Map<String,Fighter>();
  return async function selectFighterForBattle(event : MouseEvent, fighter : Fighter) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>event.target).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else { 
      selectedFighters.delete(fighter._id);
    }
    if (selectedFighters.size === 2) {
      const winner : Fighter = fight(...selectedFighters.values() as unknown as [Fighter, Fighter] );
      showWinnerModal(winner);
    }
  }
}
